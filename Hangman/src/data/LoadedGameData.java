package data;

import java.util.Set;

/**
 * @author Thomas Biscardi
 */
public class LoadedGameData {

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    private boolean        hasHint;
    private boolean        hintUsed;

    public LoadedGameData() {

    }

    public LoadedGameData(String targetWord, Set<Character> goodGuesses, Set<Character> badGuesses, int remainingGuesses) {
        this.targetWord = targetWord;
        this.goodGuesses = goodGuesses;
        this.badGuesses = badGuesses;
        this.remainingGuesses = remainingGuesses;
    }

    public LoadedGameData(GameData gamedata) {
        this.targetWord = gamedata.getTargetWord();
        this.goodGuesses = gamedata.getGoodGuesses();
        this.badGuesses = gamedata.getBadGuesses();
        this.remainingGuesses = gamedata.getRemainingGuesses();
        this.hasHint = gamedata.getHasHint();
        this.hintUsed = gamedata.getHintUsed();
    }

    public String getTargetWord() {
        return targetWord;
    }

    public void setTargetWord(String targetWord) {
        this.targetWord = targetWord;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public void setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public void setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void setRemainingGuesses(int remainingGuesses) {
        this.remainingGuesses = remainingGuesses;
    }

    public boolean getHintUsed() { return hintUsed; }
}
