package data;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee, Thomas Biscardi
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    private boolean        hasHint;
    private boolean        hintUsed;

    public GameData() {

    }

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.hasHint = checkHasHint();
        this.hintUsed = false;
    }

    public GameData(LoadedGameData lgd, AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.targetWord = lgd.getTargetWord();
        this.goodGuesses = lgd.getGoodGuesses();
        this.badGuesses = lgd.getBadGuesses();
        this.remainingGuesses = lgd.getRemainingGuesses();
        this.hasHint = checkHasHint();
        this.hintUsed = lgd.getHintUsed();
    }

    public boolean checkHasHint() {
        if(targetWord.length() <= 7) {
            return false;
        }
        char[] c = targetWord.toCharArray();
        int count = targetWord.length();
        for (int i = 0; i < c.length; i++) {
            if (i != targetWord.indexOf(c[i])) {
                count--;
            }
        }
        if(count <= 7) {
            return false;
        } else {
            return true;
        }
    }

    public void setHintUsed() { hintUsed = true; }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            for (int i = 0; i < word.length(); i++) {
                if (!Character.isLetter(word.charAt(i))) {
                    return getTargetWord();
                }
            }
            return word;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }

    public void decrementRemainingGuesses() {
        remainingGuesses--;
    }

    public boolean getHasHint() { return hasHint; }

    public boolean getHintUsed() { return hintUsed; }

    public void setAppTemplate(AppTemplate at) {
        appTemplate = at;
    }

}
