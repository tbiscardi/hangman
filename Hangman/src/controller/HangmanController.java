package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import data.GameData;
import data.LoadedGameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static hangman.HangmanProperties.ACCEPTED_LETTERS;
import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee, Thomas Biscardi
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private TextField[] progress;    // reference to the text area for the word
    private TextField[] letters;
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private GridPane    gp;
    private Canvas      canvas;
    private GraphicsContext gc;
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private boolean     needsSave;   //keeps track of whether a change to a game has been made
    private boolean     isNew;
    private Path        workFile;
    private Workspace   gameWorkspace;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
//        appTemplate.getGUI().update(this);
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
        savable = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
    }

    public void start() {
        PropertyManager props = PropertyManager.getManager();
        isNew = false;
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        Insets insets = new Insets(10, 30, 10, 0);
        remainingGuessBox.setPadding(insets);
        guessedLetters.setPadding(insets);
        gp = gameWorkspace.getAllLettersPane();
        int x = 0;
        int y = 0;
        letters = new TextField[props.getPropertyValue(ACCEPTED_LETTERS).length()];
        for(int i = 0; i < letters.length; i ++) {
            if(i % 7 == 0) {
                x = 0;
                y++;
            }
            letters[i] = new TextField(Character.toString(props.getPropertyValue(ACCEPTED_LETTERS).charAt(i)).toUpperCase());
            styleTextField(letters[i], 26);
            letters[i].setStyle("-fx-text-inner-color: white; -fx-control-inner-background: grey;");
            GridPane.setConstraints(letters[i], x, y);
            gp.getChildren().add(letters[i]);
            x++;
        }
        gp.setPadding(insets);
        if(gamedata.checkHasHint()) {
            hintButton = gameWorkspace.getHintButton();
            gameWorkspace.addHintButton();
            if(gamedata.getHintUsed() == true) {
                hintButton.setDisable(true);
            }
            hintButton.setOnAction(e -> {
                hint();
            });
        }

        canvas = gameWorkspace.getCanvas();
        gc = gameWorkspace.getGc();
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(3);
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());



        gameWorkspace.disableStart();

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        needsSave = true;

        play();
    }

    public void loadStart() {
        PropertyManager props = PropertyManager.getManager();
        isNew = false;
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        Insets insets = new Insets(10, 30, 10, 0);
        remainingGuessBox.setPadding(insets);
        guessedLetters.setPadding(insets);
        gp = gameWorkspace.getAllLettersPane();
        int x = 0;
        int y = 0;
        letters = new TextField[props.getPropertyValue(ACCEPTED_LETTERS).length()];
        for(int i = 0; i < letters.length; i ++) {
            if(i % 7 == 0) {
                x = 0;
                y++;
            }
            letters[i] = new TextField(Character.toString(props.getPropertyValue(ACCEPTED_LETTERS).charAt(i)).toUpperCase());
            styleTextField(letters[i], 26);
            if(gamedata.getBadGuesses().contains(letters[i].getText().toLowerCase().charAt(0))) {
                letters[i].setStyle("-fx-text-inner-color: white; -fx-control-inner-background: red;");
            } else if(gamedata.getGoodGuesses().contains(letters[i].getText().toLowerCase().charAt(0))) {
                letters[i].setStyle("-fx-text-inner-color: white; -fx-control-inner-background: green;");
            } else {
                letters[i].setStyle("-fx-text-inner-color: white; -fx-control-inner-background: grey;");
            }
            GridPane.setConstraints(letters[i], x, y);
            gp.getChildren().add(letters[i]);
            x++;
        }
        gp.setPadding(insets);
        if(gamedata.checkHasHint()) {
            hintButton = gameWorkspace.getHintButton();
            gameWorkspace.addHintButton();
            if(gamedata.getHintUsed() == true) {
                hintButton.setDisable(true);
            }
            hintButton.setOnAction(e -> {
                hint();
            });
        }
        canvas = gameWorkspace.getCanvas();
        gc = gameWorkspace.getGc();
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(3);
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        renderGraphics();
        gameWorkspace.disableStart();

        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        loadWordGraphics(guessedLetters);
        needsSave = false;
        savable = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        play();
    }

    private void hint() {
        gameWorkspace.disableHintButton();
        gamedata.setHintUsed();
        gamedata.decrementRemainingGuesses();
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        renderGraphics();
        ArrayList<Character> temp = new ArrayList<>();
        for(int i = 0; i < gamedata.getTargetWord().length(); i ++) {
            if(!temp.contains(gamedata.getTargetWord().charAt(i))) {
                temp.add(gamedata.getTargetWord().charAt(i));
            }
        }
        for(int i = 0; i < temp.size(); i ++) {
            if(gamedata.getGoodGuesses().contains(temp.get(i))) {
                temp.remove(i);
                i--;
            }
        }
        int random = (int)(Math.random() * temp.size());
        char hint = temp.get(random);
        for(int i = 0; i < gamedata.getTargetWord().length(); i ++) {
            if(gamedata.getTargetWord().charAt(i) == hint) {
                progress[i].setPromptText(Character.toString(hint).toUpperCase());
                progress[i].setStyle("-fx-prompt-text-fill: white; -fx-control-inner-background: black; -fx-opacity: 1.0;");
            }
        }

        gamedata.addGoodGuess(hint);
        discovered++;
        for(int i = 0; i < letters.length; i ++) {
            if (letters[i].getText().equals(Character.toString(hint).toUpperCase())) {
                letters[i].setStyle("-fx-control-inner-background: green;");
                break;
            } else {

            }
        }
        savable = true;
        success = (discovered == progress.length);
        if(!success) {
            needsSave = true;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
        }
    }

    private void end(){
//        gameWorkspace.getMessage().setText(success ? "You win!" : "You lose! The word was \"" + gamedata.getTargetWord() + "\".");
        if(hintButton != null) {
            hintButton.setDisable(true);
        }
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        Platform.runLater(() -> {
            try {
                if(success) {
                    dialog.show(props.getPropertyValue(RESULTS_TITLE), props.getPropertyValue(SUCCESS_MESSAGE));
                } else {
                    dialog.show(props.getPropertyValue(RESULTS_TITLE), props.getPropertyValue(FAILURE_MESSAGE));
                }
            } catch (IllegalStateException e) {

            }
        });
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
    }

    private void initWordGraphics(HBox guessedLetters) {
        progress = new TextField[gamedata.getTargetWord().length()];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new TextField();
            styleTextField(progress[i], 26);
            progress[i].setStyle("-fx-prompt-text-fill: white; -fx-control-inner-background: black; -fx-opacity: 1.0;");
        }
        guessedLetters.getChildren().addAll(progress);
    }

    private void loadWordGraphics(HBox guessedLetters) {
        progress = new TextField[gamedata.getTargetWord().length()];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new TextField();
            styleTextField(progress[i], 26);
            progress[i].setStyle("-fx-prompt-text-fill: white; -fx-control-inner-background: black; -fx-opacity: 1.0;");
            if(gamedata.getGoodGuesses().contains(gamedata.getTargetWord().charAt(i))) {
                progress[i].setPromptText(Character.toString(gamedata.getTargetWord().charAt(i)).toUpperCase());
                discovered++;
            } else {
                progress[i].setPromptText("");
            }
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                PropertyManager props = PropertyManager.getManager();
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    if(!isNew) {
                        char guess = event.getCharacter().toLowerCase().charAt(0);
                        if (Character.isLetter(guess)) {
                            if (!alreadyGuessed(guess)) {
                                needsSave = true;
                                boolean goodguess = false;
                                for (int i = 0; i < progress.length; i++) {
                                    if (gamedata.getTargetWord().charAt(i) == guess) {
                                        progress[i].setPromptText(Character.toString(guess).toUpperCase());
                                        progress[i].setStyle("-fx-prompt-text-fill: white; -fx-control-inner-background: black; -fx-opacity: 1.0;");
                                        gamedata.addGoodGuess(guess);
                                        goodguess = true;
                                        discovered++;
                                    }
                                }
                                if (!goodguess) {
                                    for(int i = 0; i < letters.length; i ++) {
                                        if (letters[i].getText().equals(Character.toString(guess).toUpperCase())) {
                                            letters[i].setStyle("-fx-control-inner-background: red;");
                                            break;
                                        } else {

                                        }
                                    }
                                    gamedata.addBadGuess(guess);
                                    renderGraphics();
                                } else {
                                    for(int i = 0; i < letters.length; i ++) {
                                        if (letters[i].getText().equals(Character.toString(guess).toUpperCase())) {
                                            letters[i].setStyle("-fx-control-inner-background: green;");
                                            break;
                                        } else {

                                        }
                                    }
                                }

                                success = (discovered == progress.length);
                                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                                savable = true;
                                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                            }
                        } else {
                            needsSave = false;
                        }
                    }
                });
                if(gamedata.getRemainingGuesses() <= 0) {
                    for (int i = 0; i < progress.length; i++) {
                        if(progress[i].getPromptText().trim().isEmpty()) {
                            progress[i].setPromptText(Character.toString(gamedata.getTargetWord().charAt(i)).toUpperCase());
                            progress[i].setStyle("-fx-prompt-text-fill: white; -fx-control-inner-background: grey; -fx-opacity: 1.0;");
                        } else {

                        }
                    }
                }
                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void renderGraphics() {
        switch (gamedata.getRemainingGuesses()) {
            case 0:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                gc.strokeLine(175, 135, 175, 240);
                gc.strokeLine(175, 160, 130, 210);
                gc.strokeLine(175, 160, 220, 210);
                gc.strokeLine(175, 240, 145, 315);
                gc.strokeLine(175, 240, 205, 315);
                break;
            case 1:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                gc.strokeLine(175, 135, 175, 240);
                gc.strokeLine(175, 160, 130, 210);
                gc.strokeLine(175, 160, 220, 210);
                gc.strokeLine(175, 240, 145, 315);
                break;
            case 2:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                gc.strokeLine(175, 135, 175, 240);
                gc.strokeLine(175, 160, 130, 210);
                gc.strokeLine(175, 160, 220, 210);
                break;
            case 3:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                gc.strokeLine(175, 135, 175, 240);
                gc.strokeLine(175, 160, 130, 210);
                break;
            case 4:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                gc.strokeLine(175, 135, 175, 240);
                break;
            case 5:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                gc.strokeOval(135, 55, 80, 80);
                break;
            case 6:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                gc.strokeLine(175, 10, 175, 55);
                break;
            case 7:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                gc.strokeLine(70, 10, 175, 10);
                break;
            case 8:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                gc.strokeLine(70, 10, 70, 350);
                break;
            case 9:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.strokeLine(20, 350, 350, 350);
                break;
            default:
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                break;
        }
    }

    private void styleTextField(TextField tf, int size) {
        tf.setMaxWidth(size);
        tf.setEditable(false);
        tf.setDisable(false);
        tf.setMouseTransparent(true);
        tf.setFocusTraversable(false);
        tf.setVisible(true);
        tf.setShape(new Rectangle(size, size));
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }
    
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable && needsSave)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            if(gc != null) {
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            }
            needsSave = true;
            isNew = true;
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }
    
    @Override
    public void handleSaveRequest() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        PropertyManager props = PropertyManager.getManager();
        if(needsSave == true) {
            if (workFile != null) {
                LoadedGameData loadedGameData = new LoadedGameData(gamedata);
                mapper.writerWithDefaultPrettyPrinter().writeValue(workFile.toFile(), loadedGameData);
            } else {
                LoadedGameData loadedGameData = new LoadedGameData(gamedata);
                FileChooser fc = new FileChooser();
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(props.getPropertyValue(WORK_FILE_EXT_DESC), props.getPropertyValue(WORK_FILE_EXT));
                fc.getExtensionFilters().add(extFilter);
                File file = fc.showSaveDialog(gameWorkspace.getGUI().getWindow());
                if(file != null) {
                    while (!file.getName().endsWith(props.getPropertyValue(WORK_FILE_EXT_2))) {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getPropertyValue(SAVE_WRONG_EXT_TITLE), props.getPropertyValue(SAVE_WRONG_EXT_MESSAGE));
                        file = fc.showSaveDialog(gameWorkspace.getGUI().getWindow());
                        if (file == null) {
                            break;
                        }
                    }
                }
                if (file != null) {
                    mapper.writerWithDefaultPrettyPrinter().writeValue(file, loadedGameData);
                    workFile = file.toPath();
                }
            }
            if (workFile != null) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
                needsSave = false;
                savable = false;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
            }
        } else {

        }
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if(gameWorkspace == null) {
            load();
        } else {
            if (savable && needsSave) {
                load = promptToSave();
            }
            if (load) {
                load();
            }
        }
     }

    private void load() throws IOException{
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        PropertyManager           props  = PropertyManager.getManager();
        ObjectMapper mapper = new ObjectMapper();
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(props.getPropertyValue(WORK_FILE_EXT_DESC), props.getPropertyValue(WORK_FILE_EXT));
        fc.getExtensionFilters().add(extFilter);
        File file = fc.showOpenDialog(gameWorkspace.getGUI().getWindow());
        LoadedGameData loadedGameData = null;
        if(file != null) {
            try {
                loadedGameData = mapper.readValue(file, LoadedGameData.class);
                if(loadedGameData.getGoodGuesses() == null || loadedGameData.getBadGuesses() == null) {
                    throw new FileNotFoundException();
                } else if(loadedGameData.getRemainingGuesses() <= 0 || loadedGameData.getRemainingGuesses() > 10) {
                    throw new FileNotFoundException();
                } else if (loadedGameData.getHintUsed() && (loadedGameData.getRemainingGuesses() + loadedGameData.getBadGuesses().size() != 9)) {
                    throw new FileNotFoundException();
                } else if (!loadedGameData.getHintUsed() && (loadedGameData.getRemainingGuesses() + loadedGameData.getBadGuesses().size() != 10)) {
                    throw new FileNotFoundException();
                }
                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                ensureActivatedWorkspace();                            // ensure workspace is activated
                workFile = file.toPath();                              // new workspace has never been saved to a file
                gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gameWorkspace.reinitialize();
                enableGameButton();
                gamedata = new GameData(loadedGameData, appTemplate);
                if(gc != null) {
                    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                }
                loadStart();
            } catch (FileNotFoundException e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE));
            } catch (JsonParseException | JsonMappingException e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE));
            }

        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable && needsSave)
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    
    private boolean promptToSave() throws IOException {
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), props.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
        if(dialog.getSelection() != null) {
            if(dialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                handleSaveRequest();
                dialog.resetSelection();
                return true;
            } else if(dialog.getSelection().equals(YesNoCancelDialogSingleton.NO)) {
                dialog.resetSelection();
                return true;
            } else if(dialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL)) {
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

    }
}
